package com.company;

import com.company.project.context.DataContext;
import com.company.project.context.JSContext;
import com.company.project.server.Server;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        DataContext dataContext = new JSContext();
        try {
            new Server("localhost", 8010, dataContext).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
