package com.company.project.domain;

import java.util.UUID;

public class Candidate {
    private String id = UUID.randomUUID().toString();;
    private String name;
    private String photo;
    private double votes;

    public double calculate(){
        if(Votes.count != 0){
            return (votes / Votes.count) * 100;
        }
        return 0;
    }

    public Candidate() {
    }

    public Candidate(String name, String photo) {
        this.name = name;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public double getVotes() {
        return votes;
    }

    public void setVotes(double votes) {
        this.votes = votes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                ", votes=" + votes +
                '}';
    }
}
