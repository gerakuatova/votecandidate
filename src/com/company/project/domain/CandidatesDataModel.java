package com.company.project.domain;

import java.util.List;

public class CandidatesDataModel {
    private Candidate winner;
    private List<Candidate> candidates;

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public CandidatesDataModel(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public Candidate getWinner() {
        return winner;
    }

    public void setWinner(Candidate winner) {
        this.winner = winner;
    }
}
