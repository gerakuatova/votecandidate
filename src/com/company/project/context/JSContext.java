package com.company.project.context;

import com.company.project.domain.Candidate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JSContext implements DataContext{
    private final Gson gson;
    private final String path = "data/json/candidates.json";

    public JSContext() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    public boolean add(Candidate candidate) {
        try(FileReader fileReader = new FileReader(path)){
            List<Candidate> candidateList = gson.fromJson(fileReader, new TypeToken<List<Candidate>>(){}.getType());
            if(candidateList == null){
                candidateList = new ArrayList<>();
            }
            candidateList.add(candidate);
            FileWriter writer = new FileWriter(path);
            gson.toJson(candidateList, writer);
            writer.flush();
            writer.close();
            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Candidate> getCandidate() {
        try(FileReader fileReader = new FileReader(path)){
            List<Candidate> candidateList = gson.fromJson(fileReader, new TypeToken<List<Candidate>>(){}.getType());
            return candidateList;
        }catch (IOException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
