package com.company.project.context;

import com.company.project.domain.Candidate;
import java.util.List;

public interface DataContext {
    boolean add(Candidate candidate);
    List<Candidate> getCandidate();
}
