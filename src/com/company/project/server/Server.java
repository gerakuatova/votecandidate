package com.company.project.server;

import com.company.Utils;
import com.company.project.context.DataContext;
import com.company.project.domain.Candidate;
import com.company.project.domain.CandidateDataModel;
import com.company.project.domain.CandidatesDataModel;
import com.company.project.domain.Votes;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Server extends BasicServer{
    private final List<Candidate> candidates;
    public Server(String host, int port, DataContext context) throws IOException {
        super(host, port);
        this.candidates = context.getCandidate();
        registerGet("/", this::rootHandler);
        registerGet("/votes",this::votesHandler);
        registerGet("/thankyou", this::thankyouHandler);
        registerPost("/vote", this::voteHandler);
    }

    private void voteHandler(HttpExchange httpExchange) {
        String body = getBody(httpExchange);
        Map<String, String> content = Utils.parseUrlEncoded(body, "&");
        String id = content.get("id");
        Optional<Candidate> optCandidate = candidates.stream().filter(u -> u.getId().equals(id)).findFirst();
        if(optCandidate.isPresent()){
            Candidate candidate = optCandidate.get();
            candidate.setVotes(candidate.getVotes() + 1);
            Votes.count++;
        }
        redirect303(httpExchange, "/thankyou?id="+id);

    }

    private void thankyouHandler(HttpExchange httpExchange) {
        String queryParams = getQueryParams(httpExchange);
        Map<String, String> content = Utils.parseUrlEncoded(queryParams, "&");
        String id = content.get("id");
        Optional<Candidate> optCandidate = candidates.stream().filter(u -> u.getId().equals(id)).findFirst();
        CandidateDataModel candidateDataModel = new CandidateDataModel();
        if(optCandidate.isPresent()){
            Candidate candidate = optCandidate.get();
            candidateDataModel.setCandidate(candidate);
        }
        renderTemplate(httpExchange, "thankyou.html", candidateDataModel);
    }

    private void votesHandler(HttpExchange httpExchange) {
        List<Candidate> result = candidates.stream()
                .sorted(Comparator.comparingDouble(Candidate::getVotes).reversed())
                .collect(Collectors.toList());

        var winner = result.get(0);
        result.remove(winner);
        var dataModel = new CandidatesDataModel(result);
        dataModel.setWinner(winner);
        renderTemplate(httpExchange, "votes.html", dataModel);
    }

    private void rootHandler(HttpExchange httpExchange) {
        CandidatesDataModel candidatesDataModel = new CandidatesDataModel(candidates);
        renderTemplate(httpExchange, "candidates.html", candidatesDataModel);
    }
}
